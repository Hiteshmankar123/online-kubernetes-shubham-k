# Kubernetes

## What is kubernetes?

## Why we use kubernetes?

## Kubernetes Architecture

## Kubernetes Objects:
- Nodes: Worker node
- Pods: Smallest deployable entity of k8s. Wrapper around container. Every pod has its own unique IP address
- Service: Expose application inside / outside of the cluster
    -   ClusterIP: Expose applciation inside the cluster. Used for inside communication only.
    -   NodePort: Expose application outside the cluster using Node Port. Can access application by NODEIP:NODEPORT
    -   LoadBalancer: Expose application outside the cluster on LoadBalancer Service 
- Namespace: Devide the k8s cluster. Segrigate the resources.
- ReplicationController: Responsible for creating replicas of the pods. Used equality based selector. apiVersion: v1
- ReplicaSet: Responsible for creating replicas of the pods. Used set-based selector. Multiple label selector is possible. apiVersion: apps/v1. Set Operators: In, NotIn, Exist
- Deployment: Deployment is responsible for implementing deployment strategies on replicaset.
- StatefulSet: Responsible for managing the replicas of the stateful pod.
- DaemonSet: Responsible for managing the replicas of the pods on every Node
- ConfigMap: To store variables
- Secrets: To store confidentials variables


## Example

Frontend -> Backend -> Database

index.html
Studentapp.war 
mariadb


EKS Cluster
Node group
Docker

### 3-tier modern application deployment
STEP1: Create DATABASE Server and Import Schema

STEP2: Update DATABASE DETAILS in context.xml 
STEP3: Write Dockerfile for backend-app
STEP4: Build Backend-app image and Push to registry

STEP4: Create Deployment.yaml and Service.yaml file for backend
STEP5: Deploy backend-app on cluster

STEP6: Update ClusterIP and Path in proxy.conf
STEP7: Write Dockerfile for frontend-app
STEP8: Build Frontend-app image and Push to Registry

STEP9: Create Deployment.yaml and Service.yaml file for frontend
STEP10: Deploy frontend-app on cluster

